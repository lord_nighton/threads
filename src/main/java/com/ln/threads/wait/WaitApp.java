package com.ln.threads.wait;

public class WaitApp {

    public static void main(String[] args) throws InterruptedException {
        SharedCounter counter = new SharedCounter();

        WaitingThread waiter = new WaitingThread(counter);
        SharedCounterIncrementor incrementor1 = new SharedCounterIncrementor(counter);
        SharedCounterIncrementor incrementor2 = new SharedCounterIncrementor(counter);

        Thread [] threads = {waiter, incrementor1, incrementor2};
        for (Thread next : threads) {
            next.start();
        }

        System.out.println("Main thread message before join...");

        for (Thread next : threads) {
            next.join();
        }

        System.out.println("End of MAIN thread");
    }

}