package com.ln.threads.wait;

public class WaitingThread extends Thread {

    private SharedCounter counter;

    public WaitingThread(SharedCounter counter) {
        super();
        this.counter = counter;
    }

    @Override
    public void run() {
        if (SharedCounter.WAITING_COUNT > counter.getCounter()) {
            synchronized(counter) {
                try {
                    System.out.println("Waiting thread is waiting...");
                    counter.wait();
                    System.out.println("Waiting thread is notified. Counter value = " + counter.getCounter());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}