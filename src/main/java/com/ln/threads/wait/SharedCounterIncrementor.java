package com.ln.threads.wait;

public class SharedCounterIncrementor extends Thread {

    private SharedCounter counter;

    public SharedCounterIncrementor(SharedCounter counter) {
        super();
        this.counter = counter;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < SharedCounter.WAITING_COUNT; i++) {
            if (SharedCounter.WAITING_COUNT == counter.getCounter()) {
                synchronized (counter) {
                    counter.notifyAll();
                }
            } else {
                counter.incCounter();
            }
        }
    }

}