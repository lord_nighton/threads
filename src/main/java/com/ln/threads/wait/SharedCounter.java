package com.ln.threads.wait;

public class SharedCounter {

    public static final int WAITING_COUNT = 10;

    private int counter = 0;

    public synchronized int getCounter() {
        return counter;
    }

    public synchronized void setCounter(int counter) {
        this.counter = counter;
    }

    public synchronized void incCounter(){
        this.counter++;
    }

    public synchronized void decCounter(){
        this.counter--;
    }

}