package com.ln.threads.counter;

public class CounterDecrementingRunnable implements Runnable {

    private SharedCounter sharedCounter;

    public SharedCounter getSharedCounter() {
        return sharedCounter;
    }

    public void setSharedCounter(SharedCounter sharedCounter) {
        this.sharedCounter = sharedCounter;
    }

    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println("Name = " + Thread.currentThread().getName() + ", Before DEC state = " + sharedCounter.getCounter());
            sharedCounter.decCounter();
            System.out.println("Name = " + Thread.currentThread().getName() + ", After DEC state = " + sharedCounter.getCounter());

            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}