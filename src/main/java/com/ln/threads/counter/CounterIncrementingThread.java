package com.ln.threads.counter;

public class CounterIncrementingThread extends Thread {

    private SharedCounter sharedCounter;

    public CounterIncrementingThread(final SharedCounter sharedCounter) {
        super();
        this.sharedCounter = sharedCounter;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println("Name = " + Thread.currentThread().getName() + ", Before INC state = " + sharedCounter.getCounter());
            sharedCounter.incCounter();
            System.out.println("Name = " + Thread.currentThread().getName() + ", After INC state = " + sharedCounter.getCounter());

            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}