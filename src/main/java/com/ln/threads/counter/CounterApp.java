package com.ln.threads.counter;

public class CounterApp {

    public static final String THREAD = "Thread";
    public static final String RUNNABLE = "Runnable";

    public static void main(String[] args) {
        final SharedCounter counter = new SharedCounter();

        Thread incrementor = createIncrementor(counter);
        Thread decrementor = createDecrementor(counter);

        System.out.println("Starting threads");

        incrementor.start();
        decrementor.start();

        System.out.println("Daemon threads are started");
    }

    private static Thread createDecrementor(SharedCounter counter) {
        CounterDecrementingRunnable decrementorRunnable = new CounterDecrementingRunnable();
        decrementorRunnable.setSharedCounter(counter);

        Thread decrementor = new Thread(decrementorRunnable);
        setName(decrementor, RUNNABLE);
        setDaemonState(decrementor, false);

        return decrementor;
    }

    private static void setDaemonState(final Thread thread, final boolean state) {
        thread.setDaemon(state);
        // true  -- means that thread will die when MAIN thread is in dead state
        // false -- means that processing will continue even if main thread is dead
    }

    private static void setName(final Thread thread, final String name) {
        thread.setName(name);
    }

    private static Thread createIncrementor(final SharedCounter counter) {
        Thread incrementor = new CounterIncrementingThread(counter);
        setName(incrementor, THREAD);
        setDaemonState(incrementor, false);

        return incrementor;
    }

}