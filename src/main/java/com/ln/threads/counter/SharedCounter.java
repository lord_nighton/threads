package com.ln.threads.counter;

public class SharedCounter {

    private int counter;

    public synchronized int getCounter() {
        return counter;
    }

    public synchronized void setCounter(int counter) {
        this.counter = counter;
    }

    public synchronized void incCounter() {
        this.counter++;
    }

    public synchronized void decCounter() {
        this.counter--;
    }

}