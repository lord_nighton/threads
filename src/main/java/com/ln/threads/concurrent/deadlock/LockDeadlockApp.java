package com.ln.threads.concurrent.deadlock;

public class LockDeadlockApp {

    public static void main(String[] args) {
        final AtomicBlogNode local = new AtomicBlogNode(8080);
        final AtomicBlogNode remote = new AtomicBlogNode(9001);

        final String update1 = "1";
        final String update2 = "2";

        new Thread(new Runnable() {
            public void run() {
                local.printIds(update1, remote);
            }
        }).start();
        new Thread(new Runnable() {
            public void run() {
                remote.printIds(update2, local);
            }
        }).start();
    }

}