package com.ln.threads.concurrent.deadlock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AtomicBlogNode {

    private final int id;
    private final Lock lock = new ReentrantLock();

    public AtomicBlogNode(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void printIds(String update, AtomicBlogNode node) {
        lock.lock();
        try {
            System.out.println("Update = " + update + ", Self ID = " + this.getId() + ", Other ID = " + node.getId());
            node.confirmIds(update, this);
        } finally {
            lock.unlock();
        }
    }

    public void confirmIds(String update, AtomicBlogNode node) {
        lock.lock();
        try {
            System.out.println("Update = " + update + ", Self ID = " + this.getId() + ", Other ID = " + node.getId());
        } finally {
            lock.unlock();
        }
    }

}