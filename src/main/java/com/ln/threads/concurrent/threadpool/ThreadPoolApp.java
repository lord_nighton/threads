package com.ln.threads.concurrent.threadpool;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class ThreadPoolApp {

    private static final int WORK_COUNT = 10;

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(WORK_COUNT);
        BlockingQueue<WorkUnit<String>> queue = new LinkedBlockingQueue<WorkUnit<String>>();

        for (int i = 0; i < WORK_COUNT; i++) {
            queue.put(new WorkUnit<String>("Work #" + i));
        }

        WorkExecutor executor = new WorkExecutor(latch, queue);

        ExecutorService pool = Executors.newFixedThreadPool(2);

        for (int i = 0; i < WORK_COUNT; i++) {
            pool.execute(executor);
        }
        latch.await();
        pool.shutdown();

        System.out.println("Finished");
    }

}