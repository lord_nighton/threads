package com.ln.threads.concurrent.threadpool;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

public class WorkExecutor implements Runnable {

    private BlockingQueue<WorkUnit<String>> queue;
    private CountDownLatch latch;

    public WorkExecutor(CountDownLatch latch, BlockingQueue queue) {
        this.queue = queue;
        this.latch = latch;
    }

    public void run() {
        System.out.println("Work is = " + queue.poll().getWork());
        latch.countDown();
    }

}