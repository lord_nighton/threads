package com.ln.threads.concurrent.threadpool;

public class WorkUnit<T> {

    private T work;

    public WorkUnit(T work) {
        this.work = work;
    }

    public T getWork(){
        return this.work;
    }

}