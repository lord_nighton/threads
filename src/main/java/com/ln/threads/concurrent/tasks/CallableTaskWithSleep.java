package com.ln.threads.concurrent.tasks;

import java.util.concurrent.Callable;

public class CallableTaskWithSleep implements Callable<String> {

    private String name;
    private int millisecondsToSleep;

    public CallableTaskWithSleep(final int millisecondsToSleep, final String name) {
        this.name = name;
        this.millisecondsToSleep = millisecondsToSleep;
    }

    public String call() throws Exception {
        Thread.sleep(millisecondsToSleep);

        /*final StringBuilder sum = new StringBuilder();
        for (int i = 0; i < 10; i++){
            sum.append(name).append("_");
        }*/
        return name;
    }

}