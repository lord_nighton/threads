package com.ln.threads.concurrent.tasks;

import java.util.concurrent.Callable;

public class SimpleCallableTask implements Callable {

    private int count;
    private String name;

    public SimpleCallableTask(final int count, final String name) {
        this.name = name;
        this.count = count;
    }

    public Object call() throws Exception {
        final StringBuilder sum = new StringBuilder();
        for (int i = 0; i < this.count; i++){
            sum.append(name).append("_");
        }
        return sum.toString();
    }

}