package com.ln.threads.concurrent.tasks;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class TasksApp {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        // startSimpleTask();

        Callable<String> task1 = new CallableTaskWithSleep(100, "Nikita");
        Callable<String> task2 = new CallableTaskWithSleep(2000, "Nikita");

        FutureTask<String> future1 = new FutureTask<String>(task1);
        FutureTask<String> future2 = new FutureTask<String>(task2);

        ExecutorService pool = Executors.newFixedThreadPool(2);
        pool.execute(future1);
        pool.execute(future2);

        while (true) {
            if (future1.isDone() && future2.isDone()) {
                System.out.println("Done!");
                pool.shutdown();
                break;
            }

            if (!future1.isDone()) {
                System.out.println("Callable1 result = " + future1.get());
            }

            try {
                String result2 = future2.get(200, TimeUnit.MILLISECONDS);
                if (null != result2) {
                    System.out.println("Callable2 result = " + result2);
                }
            } catch (TimeoutException e) {
                System.out.println("Timeout exception");
            }
        }
    }

    private static void startSimpleTask() {
        Callable callableTask = new SimpleCallableTask(5, "Nikita");
        try {
            String sum = (String) callableTask.call();
            System.out.println("Sum = " + sum);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}