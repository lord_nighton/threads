package com.ln.threads.concurrent.tasks;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class IncNumberFutureTask extends FutureTask<Integer> {

    public IncNumberFutureTask(Callable<Integer> callable) {
        super(callable);
    }

    @Override
    public void run() {
        long counter = 0;
        while (true) {
            System.out.println("Counter = " + counter++);

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}