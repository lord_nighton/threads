package com.ln.threads.concurrent.countdownlatch;

import java.util.concurrent.CountDownLatch;

public class LatchThread extends Thread {

    private CountDownLatch latch;

    public LatchThread(final CountDownLatch latch) {
        super();
        this.latch = latch;
    }

    public void initialize() {
        latch.countDown();
    }

    @Override
    public void run() {
        initialize();
    }

}