package com.ln.threads.concurrent.countdownlatch;

import java.util.concurrent.CountDownLatch;

public class LatchApp {

    private static final int QUORUM = 50;
    private static final int THREADS_AMOUNT = QUORUM * 2;

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(QUORUM);

        for (int i = 0; i < THREADS_AMOUNT; i++) {
            new LatchThread(latch).start();
        }
        latch.await();

        System.out.println("Half of quorum is reached! Latch counter = " + latch.getCount());
    }

}