package com.ln.threads.concurrent.blockingqueue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

public class BlockingQueueApp {

    private static final int PETS_AMOUNT = 10;

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Appointment<Pet>> queue = new LinkedBlockingQueue<Appointment<Pet>>();

        CountDownLatch latch = new CountDownLatch(PETS_AMOUNT/2);
        for (int i = 0; i < PETS_AMOUNT / 2; i++) {
            queue.put(new Appointment<Pet>(new Dog("Cat" + i)));
            queue.put(new Appointment<Pet>(new Cat("Dog" + i)));
        }

        final Veterinar doc1 = new Veterinar(latch, queue);
        final Veterinar doc2 = new Veterinar(latch, queue);

        doc1.start();
        doc2.start();

        latch.await();

        doc1.setShutdown(true);
        doc2.setShutdown(true);

        System.out.println("End of processing queue");
    }

}