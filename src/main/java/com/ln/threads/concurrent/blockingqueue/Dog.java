package com.ln.threads.concurrent.blockingqueue;

public class Dog extends Pet {

    public Dog(String name) {
        super(name);
    }

    @Override
    public void examine() {
        System.out.println("Gav!");
    }

}