package com.ln.threads.concurrent.blockingqueue;

public class Cat extends Pet {

    public Cat(String name) {
        super(name);
    }

    @Override
    public void examine() {
        System.out.println("Meow!");
    }

}