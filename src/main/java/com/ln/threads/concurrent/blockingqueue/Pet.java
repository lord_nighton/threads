package com.ln.threads.concurrent.blockingqueue;

public abstract class Pet {

    private String name;

    public Pet(String name) {
        this.name = name;
    }

    public abstract void examine();

}