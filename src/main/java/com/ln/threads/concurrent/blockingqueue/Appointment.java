package com.ln.threads.concurrent.blockingqueue;

public class Appointment<T> {

    private final T patient;

    public T getPatient() {
        return patient;
    }

    public Appointment(T patient) {
        this.patient = patient;
    }

}