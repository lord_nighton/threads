package com.ln.threads.concurrent.blockingqueue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

public class Veterinar extends Thread {

    private static final int REST_TIME = 50;

    private boolean shutdown = false;

    private CountDownLatch latch;
    private BlockingQueue<Appointment<Pet>> queue;

    public void setShutdown(boolean shutdown) {
        this.shutdown = shutdown;
    }

    public Veterinar(CountDownLatch latch, BlockingQueue<Appointment<Pet>> queue) {
        this.queue = queue;
        this.latch = latch;
    }

    @Override
    public void run() {
        while (!shutdown) {
            try {
                Appointment<Pet> pet = queue.take();
                pet.getPatient().examine();

                Thread.sleep(REST_TIME);

                latch.countDown();
            } catch (InterruptedException e) {
                shutdown = true;
            }
        }
    }
}