package com.ln.threads.concurrent.atomic;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class ConcurrentAtomicApp {

    public static void main(String[] args) throws InterruptedException {
        final AtomicLong counter = new AtomicLong(0L);

        final List<Thread> threads = new ArrayList<Thread>();
        for (int i = 0; i < 10; i++) {
            Thread thread = new AtomicCounterIncrementor(counter);
            threads.add(thread);
        }

        for (int i = 0; i < 10; i++) {
            threads.get(i).start();
        }

        for (int i = 0; i < 10; i++) {
            threads.get(i).join();
        }

        System.out.println("Counter = " + counter.get());
    }

}