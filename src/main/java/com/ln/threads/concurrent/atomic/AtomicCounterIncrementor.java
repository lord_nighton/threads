package com.ln.threads.concurrent.atomic;

import java.util.concurrent.atomic.AtomicLong;

public class AtomicCounterIncrementor extends Thread {

    private AtomicLong counter;

    public AtomicCounterIncrementor(AtomicLong counter) {
        super();
        this.counter = counter;
    }

    @Override
    public void run() {
        counter.getAndIncrement();
    }

}