package com.ln.threads.deadlock;

public class SharedBlogNode {

    private final int id;

    public SharedBlogNode(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public synchronized void printIds(String update, SharedBlogNode node) {
        System.out.println("Update = " + update + ", Self ID = " + this.getId() + ", Other ID = " + node.getId());
        node.restoreIds(update, this);
    }

    public synchronized void restoreIds(String update, SharedBlogNode node) {
        System.out.println("Update = " + update + ", Self ID = " + this.getId() + ", Other ID = " + node.getId());
    }

}