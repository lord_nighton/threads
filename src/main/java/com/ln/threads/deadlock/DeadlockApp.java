package com.ln.threads.deadlock;

public class DeadlockApp {

    public static void main(String[] args) {
        final SharedBlogNode local = new SharedBlogNode(8080);
        final SharedBlogNode remote = new SharedBlogNode(9001);

        final String update1 = "1";
        final String update2 = "2";

        new Thread(new Runnable() {
            public void run() {
                local.printIds(update1, remote);
            }
        }).start();
        new Thread(new Runnable() {
            public void run() {
                remote.printIds(update2, local);
            }
        }).start();
    }

}