package com.ln.threads.volatilecounter;

public class DecrementCommand implements Command {

    public void executeCommand(VolatileCounter counter) {
        for (int i = 0; i < 100; i++) {
            counter.decCounter();

            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // System.out.println("After DEC --> " + counter.getCounter());
        }
    }

}