package com.ln.threads.volatilecounter;

public class CommandThread extends Thread {

    private Command command;
    private VolatileCounter counter;

    public VolatileCounter getCounter() {
        return counter;
    }

    public void setCounter(VolatileCounter counter) {
        this.counter = counter;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    @Override
    public void run() {
        command.executeCommand(counter);
    }

}