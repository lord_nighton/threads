package com.ln.threads.volatilecounter;

public interface Command {

    public void executeCommand(VolatileCounter counter);

}