package com.ln.threads.volatilecounter;

public class VolatileCounter {

    // private int counter; --> for synchronized non-volatile counter
    private volatile int counter;

    public /* synchronized */ int getCounter() {
        return counter;
    }

    public /* synchronized */ void setCounter(int counter) {
        this.counter = counter;
    }

    public /* synchronized */ void incCounter() {
        this.counter++;
    }

    public /* synchronized */ void decCounter() {
        this.counter--;
    }

}