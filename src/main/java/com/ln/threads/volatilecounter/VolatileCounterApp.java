package com.ln.threads.volatilecounter;

// http://www.ibm.com/developerworks/ru/library/j-5things15/
// This example shows an incorrect behaviour of volatile variable when inc / dec operations are fulfilled
public class VolatileCounterApp {

    public static void main(String[] args) throws InterruptedException {
        VolatileCounter counter = new VolatileCounter();

        Command incrementCommand = new IncrementCommand();
        Command decrementCommand = new DecrementCommand();

        CommandThread incrementor = new CommandThread();
        incrementor.setName("Incrementor");
        incrementor.setCommand(incrementCommand);
        incrementor.setCounter(counter);

        CommandThread decrementor = new CommandThread();
        decrementor.setName("Decrementor");
        decrementor.setCommand(decrementCommand);
        decrementor.setCounter(counter);

        incrementor.start();
        decrementor.start();

        Thread [] threads = {incrementor, decrementor};
        for (Thread thread : threads) {
            thread.join();
        }

        System.out.println("Value of counter = " + counter.getCounter());
    }

}