package com.ln.threads.volatilecounter;

public class IncrementCommand implements Command {

    public void executeCommand(VolatileCounter counter) {
        for (int i = 0; i < 100; i++) {
            counter.incCounter();

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // System.out.println("After INC --> " + counter.getCounter());
        }
    }

}